﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStandartAttackBehavior : CharacterBaseState
{
    private Transform target;
    public float rotationSpeed;
    public float fireCountDown;
    public float fireRate;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        target = GetCharacterController(animator).target;
        fireCountDown = 0.15f;
        //create function to make random waitTime
    }


    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        UpdateTarget(animator);
        if (GetCharacterController(animator).moveJoystick.Vertical != 0 && GetCharacterController(animator).moveJoystick.Horizontal != 0)
        {
            animator.SetTrigger("Walking");
            return;
        }
        if (target == null)
        {
            return;
        }
        Aim(animator);
        if (fireCountDown <= 0f)
        {
            Shoot(animator);
            fireCountDown = 1f / fireRate;
        }
        fireCountDown -= Time.deltaTime;
    }

    private void Shoot(Animator animator)
    {
        GetCharacterController(animator).Shoot();
    }

    private void Aim(Animator animator)
    {
        Vector3 dir = target.position - GetCharacterController(animator).transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(GetCharacterController(animator).characterRotation.rotation, lookRotation, Time.deltaTime * rotationSpeed).eulerAngles;
        GetCharacterController(animator).characterRotation.rotation = Quaternion.Euler(0f, rotation.y, 0f);
    }

    private void UpdateTarget(Animator animator)
    {
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        foreach (GameObject enemyTarget in GetCharacterController(animator).targets)
        {
            if (enemyTarget == null)
                continue;
            float distanceToEnemy = Vector3.Distance(GetCharacterController(animator).transform.position, enemyTarget.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemyTarget;
            }
        }

        if (nearestEnemy != null && shortestDistance <= GetCharacterController(animator).range)
        {
            target = nearestEnemy.transform;
        }
        else
        {
            target = null;
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
