﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class CharacterController : MonoBehaviour
{
    [Header("ALL CHARACTERS")]

    public Animator animator;
    public Transform characterRotation;
    public GameObject projectilePrefab;
    public GameObject[] targets;
    public Transform firePoint;
    public float range = 5f;
    public float startHealth = 100;
    public float health;
    public Image healthBar;
    public GameObject deathEffect;

    [Header("ENEMY CHARACTERS")]

    public Transform wayPoint = null;
    public Transform[] FirePointsArch = null;
    public Transform[] FirePointsCross = null;
    public GameObject schieldPrefab = null;
    public Transform schieldSpawn = null;

    [Header("PLAYER CHARACTER")]

    public FixedJoystick moveJoystick = null;

    [HideInInspector]
    public Transform target = null;
    public static Action PlayerDied;
    public static Action PlayerSurvived;

    private void Start()
    {
        health = startHealth;
    }
    private void FixedUpdate()
    {
        Health();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Projectile")
        {
            TakeDamage(2);
        }
    }
    public void FastFire()
    {
        GameObject Projectile = Instantiate(projectilePrefab, firePoint.position, firePoint.rotation);
        StartCoroutine(DelayedFire(0.2f));
        StartCoroutine(DelayedFire(0.4f));
    }

    private IEnumerator DelayedFire(float time)
    {
        yield return new WaitForSeconds(time);
        GameObject Projectile = Instantiate(projectilePrefab, firePoint.position, firePoint.rotation);
    }
    public void TakeDamage(float amount)
    {
        health -= amount;
        healthBar.fillAmount = health / startHealth;
    }

    public void Shoot()
    {
        GameObject Projectile = Instantiate(projectilePrefab, firePoint.position, firePoint.rotation);
    }

    public void Health()
    {
        if (health <= 0)
        {
            animator.SetTrigger("Died");
            Destroy(gameObject, 0.2f);
        }
    }

    public void CrossFire()
    {
        foreach (Transform firePoint in FirePointsCross)
        {
            GameObject Projectile = Instantiate(projectilePrefab, firePoint.position, firePoint.rotation);
        }
    }

    public void ArchFire()
    {
        foreach (Transform firePoint in FirePointsArch)
        {
            GameObject Projectile = Instantiate(projectilePrefab, firePoint.position, firePoint.rotation);
        }
    }

    public void Shield()
    {
        GameObject Schield = Instantiate(schieldPrefab, schieldSpawn.position, schieldSpawn.rotation);
        Schield.transform.parent = transform;
        Schield.GetComponent<CharacterSchield>().SchieldStrenghGetter(startHealth);
    }

    //Move Faster

    

}
