﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIdleState : CharacterBaseState
{
    private Transform target;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        target = GetCharacterController(animator).target;

    }


    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (GetCharacterController(animator).moveJoystick.Vertical != 0 && GetCharacterController(animator).moveJoystick.Horizontal != 0)
        {
            animator.SetBool("Idle", false);
            animator.SetBool("Walking", true);
        }
        UpdateTarget(animator);
        if (target != null)
        {
            animator.SetBool("EndAttack", false);
            animator.SetBool("Attack", true);
        }
    }

    private void UpdateTarget(Animator animator)
    {
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        foreach (GameObject enemyTarget in GetCharacterController(animator).targets)
        {
            if (enemyTarget == null)
                continue;
            float distanceToEnemy = Vector3.Distance(GetCharacterController(animator).transform.position, enemyTarget.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemyTarget;
            }
        }

        if (nearestEnemy != null && shortestDistance <= GetCharacterController(animator).range)
        {
            target = nearestEnemy.transform;
        }
        else
        {
            target = null;
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
