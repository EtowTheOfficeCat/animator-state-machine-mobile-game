﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBaseState : StateMachineBehaviour
{
    private CharacterController characterController;
    public CharacterController GetCharacterController(Animator animator)
    {
        if (characterController == null)
        {
            characterController = animator.GetComponent<CharacterController>();
        }
        return characterController;
    }

}
