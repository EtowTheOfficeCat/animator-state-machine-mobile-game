﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRunningState : CharacterBaseState
{
    public float speed = 0.01f;
    public float rotationSpeed = 10f;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {


    }


    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        Movement(animator);

        if (GetCharacterController(animator).moveJoystick.Vertical == 0 && GetCharacterController(animator).moveJoystick.Horizontal == 0)
        {
            animator.SetBool("Idle", true);
            animator.SetBool("Walking", false);
        }
    }


    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

    private void Movement(Animator animator)
    {
        float hoz = GetCharacterController(animator).moveJoystick.Horizontal;
        float ver = GetCharacterController(animator).moveJoystick.Vertical;
        Vector3 direction = new Vector3(hoz, 0, ver).normalized;

        if (direction != Vector3.zero)
        {
            GetCharacterController(animator).transform.rotation = Quaternion.Slerp(GetCharacterController(animator).transform.rotation, Quaternion.LookRotation(direction), rotationSpeed * Time.deltaTime);
        }

        GetCharacterController(animator).transform.Translate(direction * speed, Space.World);

    }
}
