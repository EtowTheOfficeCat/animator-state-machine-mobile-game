﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPatrolBehavior : CharacterBaseState
{
    public float speed;
    public float rotationSpeed;
    private float waitTime;
    public float startWaitTime;

    private Transform moveSpots;
    private string moveSportsTags;

    public float minX;
    public float maxX;
    public float minY;
    public float maxY;

    private Transform target;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        moveSpots = GetCharacterController(animator).wayPoint;
        waitTime = startWaitTime;
        moveSpots.position = new Vector3(Random.Range(minX, maxX), 0f, Random.Range(minY, maxY));
        target = GetCharacterController(animator).target;
    }

    
   override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
   {
        
        animator.transform.position = Vector3.MoveTowards(animator.transform.position, moveSpots.position, speed * Time.deltaTime);
        animator.transform.LookAt(moveSpots.position);
        
        if (Vector3.Distance(animator.transform.position, moveSpots.position) < 0.2f)
        {
            animator.SetTrigger("BaseAttack");
        }
   }

    private void UpdateTarget(Animator animator)
    {
        float shortestDistance = Mathf.Infinity;
        GameObject nearestEnemy = null;
        foreach (GameObject enemyTarget in GetCharacterController(animator).targets)
        {
            if (enemyTarget == null)
                continue;
            float distanceToEnemy = Vector3.Distance(GetCharacterController(animator).transform.position, enemyTarget.transform.position);
            if (distanceToEnemy < shortestDistance)
            {
                shortestDistance = distanceToEnemy;
                nearestEnemy = enemyTarget;
            }
        }

        if (nearestEnemy != null && shortestDistance <= GetCharacterController(animator).range)
        {
            target = nearestEnemy.transform;
        }
        else
        {
            target = null;
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
