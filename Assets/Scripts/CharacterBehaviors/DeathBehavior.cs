﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathBehavior : CharacterBaseState
{
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GameObject Deatheffect = Instantiate(GetCharacterController(animator).deathEffect, GetCharacterController(animator).firePoint.position, GetCharacterController(animator).firePoint.rotation);
        Destroy(Deatheffect, 1f);
    }

     override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

     
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

   
}
