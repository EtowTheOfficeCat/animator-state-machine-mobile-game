﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegainHealthBehavior : CharacterBaseState
{
    public float healAmount = 15;
    private float _health;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GetCharacterController(animator).health += healAmount;
        if (GetCharacterController(animator).health > GetCharacterController(animator).startHealth)
        {
            GetCharacterController(animator).health = GetCharacterController(animator).startHealth;
        }
        GetCharacterController(animator).healthBar.fillAmount = GetCharacterController(animator).health / GetCharacterController(animator).startHealth;
        animator.SetTrigger("BaseAttack");
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }

     
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        
    }
}
