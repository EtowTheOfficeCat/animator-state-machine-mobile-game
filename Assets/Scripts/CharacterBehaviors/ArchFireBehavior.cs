﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArchFireBehavior : CharacterBaseState
{
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GetCharacterController(animator).ArchFire();
        animator.SetTrigger("BaseAttack");
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }


    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
