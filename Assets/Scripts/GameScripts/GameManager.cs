﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Button fastFireButton;
    public Button SpeedButton;

    public Animator Player;
    public Animator Enemy1;
    public Animator Enemy2;

    public GameObject VictoryMenu;
    public GameObject GameOverMenu;

    public float fastFireTimer = 10f;
    private float _fastFireTimer;

    public float healTimer = 10f;
    private float _healtTimer;

    public float schieldTimer = 10f;
    private float _schieldTimer;

    public float speedBoostTimer = 25f;
    private float _speedBoostTimer;

    public float crossFireTimer = 3f;
    private float _crossFireTimer;

    public float archFireTimer = 5f;
    private float _archFireTimer;

    private void Awake()
    {
        Time.timeScale = 1;
    }
    void Start()
    {
        _fastFireTimer = fastFireTimer;
        _healtTimer = healTimer;
        _schieldTimer = schieldTimer;
        _speedBoostTimer = speedBoostTimer;
        _archFireTimer = archFireTimer;
        _crossFireTimer = crossFireTimer;

        SpeedButton.interactable = false;
        fastFireButton.interactable = false;
    }

    
    void Update()
    {
        FastFire();
        Heal();
        Schield();
        SpeedBoost();
        CrossFire();
        ArchFire();
        CheckGameWinCondition();
    }

    private void CheckGameWinCondition()
    {
        if(Enemy1 == null && Enemy2 == null)
        {
            VictoryMenu.SetActive(true);
            Time.timeScale = 0;
        }
        if(Player == null)
        {
            GameOverMenu.SetActive(true);
            Time.timeScale = 0;
        }
    }

    void FastFire()
    {
        if(fastFireTimer <= 0)
        {
            fastFireButton.interactable = true;
            return;
        }
        fastFireTimer -= Time.deltaTime;
    }

    public void FastFireButton()
    {
        fastFireButton.interactable = false;
        if (Player == null)
            return;
        Player.SetTrigger("FastFire");
        fastFireTimer = _fastFireTimer;
    }

    void SpeedBoost()
    {
        if (speedBoostTimer <= 0)
        {
            SpeedButton.interactable = true;
            return;
        }
        speedBoostTimer -= Time.deltaTime;
    }

    public void SpeedBoostButton()
    {
        SpeedButton.interactable = false;
        if (Player == null)
            return; ;
        StartCoroutine(SpeedBoostDuration(5f));
        speedBoostTimer = _speedBoostTimer;
    }

    public IEnumerator SpeedBoostDuration(float duration)
    {
        Player.SetTrigger("SpeedBoost");
        yield return new WaitForSeconds(duration);
        Player.SetTrigger("SpeedBoostEnded");
    }

    void Heal()
    {
        if (healTimer <= 0)
        {
            SpeedButton.interactable = true;
            if (Enemy2 == null)
                return;
            Enemy2.SetTrigger("Heal");
            healTimer = _healtTimer;
        }
        healTimer -= Time.deltaTime;
    }

   


    void Schield()
    {
        if(schieldTimer <= 0)
        {
            if (Enemy1 == null)
                return;
            Enemy1.SetTrigger("ActivateSchield");
            schieldTimer = _schieldTimer;
        }
        schieldTimer -= Time.deltaTime;
    }

   

    void CrossFire()
    {
        if(crossFireTimer <= 0)
        {
            if (Enemy1 == null)
                return;
            Enemy1.SetTrigger("CrossFire");
            crossFireTimer = _crossFireTimer;
        }
        crossFireTimer -= Time.deltaTime;
    }

    void ArchFire()
    {
        if(archFireTimer <= 0)
        {
            if (Enemy2 == null)
                return;
            Enemy2.SetTrigger("ArchFire");
            archFireTimer = _archFireTimer;
        }
        archFireTimer -= Time.deltaTime;
    }
}
