﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSchield : MonoBehaviour
{
    private float schieldStrengh;
    public float schielTimer = 3f;
    private float _schielTimer;

    private void Awake()
    {
        _schielTimer = schielTimer;
    }
    private void Update()
    {
        if(schieldStrengh <= 0)
        {
            Destroy(gameObject);
        }
        if(schielTimer <= 0)
        {
            Destroy(gameObject);
        }
        schielTimer -= Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Projectile")
        {
            TakeDamage(2);
        }
    }

    public void SchieldStrenghGetter(float _schieldStrengh)
    {
        schieldStrengh = _schieldStrengh / 4;
    }

    public void TakeDamage(float amount)
    {
        schieldStrengh -= amount;
        //healthBar.fillAmount = health / startHealth;
    }
}
